SRCS=$(wildcard *.tex)

SRC=main
OUTEXT=pdf

TMP=*.aux *.log *.dvi *.toc *.bbl *.blg *.out *~

COMPIL=xelatex
BIB=bibtex

all : $(SRC)

clean :
	rm $(cat .gitignore | sed ':a;N;$!ba;s/\n/ /g')
#	rm -f $(TMP) *.$(OUTEXT)
	
$(SRC) : $(SRCS)
	xelatex -synctex=1 -interaction=nonstopmode $(SRC).tex
	#$(COMPIL) $(SRC).tex
	#texindy -I latex -C utf8 -L french $(SRC).idx
	#makeglossaries $(SRC)
	#$(COMPIL) $(SRC).tex
	#$(BIB) $(SRC)
	bibtex $(SRC)
	#makeglossaries $(SRC)
	xelatex -synctex=1 -interaction=nonstopmode $(SRC).tex
	xelatex -synctex=1 -interaction=nonstopmode $(SRC).tex
	#$(COMPIL) $(SRC).tex
	#$(COMPIL) $(SRC).tex
