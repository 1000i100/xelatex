[![build status](https://framagit.org/1000i100/xelatex/badges/master/build.svg)](https://framagit.org/1000i100/xelatex/commits/master)

## [Télécharger la dernière version fonctionnnelle](https://framagit.org/1000i100/xelatex/builds/artifacts/master/download?job=build)

# Guide d'utilisation

## Prérequis pour pouvoir travailler hors connexion :
- [TeX Live](http://www.tug.org/texlive/) ou une alternative si vous avez déjà vos habitudes
- [TeXstudio](http://www.texstudio.org/) ou une alternative si vous avez déjà vos habitudes
- Utiliser XeLaTeX et Biber pour compiler.
Expliquation de comment faire pour TeXstudio
- aller dans le menu `Options` puis `Configurer TeXstudio...`
- cliquer sur `Production` dans la colonne de gauche
- choisir dans `Compilation par défaut` : `XeLaTeX`
- choisir dans `Outil de bibliographie par défaut` : `Biber`
- Fermer la fenêtre en cliquant sur `OK`

## Travailler en ligne
Vous pouvez éditer directement vos fichers .tex depuis l'interface web de gitlab/framagit.

Quand vous sauvegardez une compilation automatisé sera déclanchée.

Vous pouvez consulter son avancement dans la [pipelines](https://framagit.org/1000i100/xelatex/pipelines)

Si tous ce passe bien, le pdf généré sera téléchargeable (empacté dans un zip) via l'icone tout à droite sur la page pipelines.

Si la compilation échoue, vous pouvez consulter le log d'execution vous informant des erreurs en cliquant sur `failed`

